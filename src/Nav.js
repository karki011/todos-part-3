import React from 'react';
import { NavLink } from 'react-router-dom';
import './nav.css';
const Nav = (props) => (
	<div className="navigation">
		<ul>
			<li className="nav-link active" id="nav">
				<NavLink exact to="/" activeClassName="selected">
					All
				</NavLink>
			</li>
			<li className="nav-link">
				<NavLink to="/active" activeClassName="selected">
					Active{' '}
				</NavLink>
			</li>
			<li className="nav-link">
				<NavLink to="/completed" activeClassName="selected">
					Completed{' '}
				</NavLink>
			</li>
		</ul>
	</div>
);

export default Nav;
